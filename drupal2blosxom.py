#!/usr/bin/env python

import migrate
import sys
import writeentries
import os

def main():
    if not len(sys.argv) == 3:
        print "Enter file name to parse"
        print "Usage : ./main.py XMLfile foldername"
        print "XMLfile = xml dump of drupal databse"
        print "foldername = folder to store txt entries in"
        sys.exit(0)

    if not os.path.isdir(sys.argv[2]):
        print "Enter a valid folder name"
        print "Usage : ./main.py XMLfile foldername"
        print "XMLfile = xml dump of drupal databse"
        print "foldername = folder to store txt entries in"
        sys.exit(0)

    foldername = sys.argv[2]

    try:
        _list=[]
        print "Parsing MYSQL dump.."
        parseDoc=migrate.openXMLdump(sys.argv[1])
        migrate.parseDocument(parseDoc,_list)
        print "Writing entries to txt files and resetting mtimes.."
        writeentries.parseAndWrite(_list, foldername)


    except:
        print "Not a valid XML file"
        print " Create a SQL dump of Drupal database using mysqldump"
        print " eg : mysqldump --xml --user=root bandanco_drupal -p | gzip > backup.sql.gz"
        sys.exit()
        


if __name__ == "__main__":
    main()
