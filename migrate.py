#!/usr/bin/env python
from xml.dom import minidom
import sys
import getopt
import os
import time
def openXMLdump(filename):
    ''' Create a SQL dump of Drupal database using mysqldump
    eg : mysqldump --xml --user=root bandanco_drupal -p | gzip > backup.sql.gz
    '''
    if not os.path.exists(filename):
        print "File does not exist!"
        sys.exit()
    fsock = open(filename)
    xmldoc = minidom.parse(fsock)
    fsock.close()
    return xmldoc

def parseDocument(doc, _list):
    ''' Parse the XML object
    '''
    tagname = doc.getElementsByTagName('table_data')
    len = tagname.length
    for count in range(len):        
        rootelem = tagname[count]
        if rootelem.attributes["name"].value == "node":
            realdata = rootelem.getElementsByTagName('row')
            realdatalen = realdata.length
            for count in range(realdatalen):
                _listelems=[]
                element = realdata[count]
                fields = element.getElementsByTagName('field')
                fieldslen = fields.length
                for count in range(fieldslen):

                    if fields[count].attributes["name"].value == "nid":
                        nodeid = fields[count].firstChild.data
                        _listelems.append(nodeid)

                    if fields[count].attributes["name"].value == "type":
                        posttype = fields[count].firstChild.data
                        if not posttype == "story":
                            break

                    if fields[count].attributes["name"].value == "title":
                        title = fields[count].firstChild.data
                        _listelems.append(title)
                    
                    if fields[count].attributes["name"].value == "created":
                        time = fields[count].firstChild.data
                        _listelems.append(time)
                        _list.append(_listelems)
                        break

        if rootelem.attributes["name"].value == "node_revisions":
            realdata = rootelem.getElementsByTagName('row')
            realdatalen = realdata.length
            for count in range(realdatalen):
                _listelems=[]
                element = realdata[count]
                fields = element.getElementsByTagName('field')
                fieldslen = fields.length
                for count in range(fieldslen):

                    if fields[count].attributes["name"].value == "nid":
                        nodeid = fields[count].firstChild.data
                                
                    if fields[count].attributes["name"].value == "body":
                        try:
                            postdata = fields[count].firstChild.data
                        except AttributeError:
                            postdata = " "
                        for elems in _list:
                            if elems.__contains__(nodeid):
                                elems.append(postdata)
                                break



