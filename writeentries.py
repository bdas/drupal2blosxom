#!/usr/bin/env python

import os
import time

def resetTimes(filename,timestamp):
    ''' Modify access time and modified time
    Actually just set modified time, Leave modified time
    untouched
    '''
    accesstime = int(time.time()) #now
    mtime = int(timestamp)
    os.utime(filename, (accesstime, mtime))

def createfileWithContents(filename, body, folder, timestamp):
    ''' create file in the specified folder
    and add body
    '''
    fullpath = folder + "/" + filename
    fsock = open(fullpath, 'w')
    #ASCII is limited to 128 in range!
    s = body.encode('utf-8')
    fsock.write(s)
    fsock.close()    
    resetTimes(fullpath, timestamp)

def parseAndWrite(_list, folder):
    ''' Get our list of title, unix timestamp, body tuples
    and put them int a file
    '''
    
    for metaentries in _list:
        title = metaentries[1]
        realtitle = title[0:4]
        timestamp = metaentries[2]
        body = metaentries[3]
        filename = realtitle.replace(" ", "_")
        filename = filename + ".HTML"
        createfileWithContents(filename, body, folder, timestamp)
